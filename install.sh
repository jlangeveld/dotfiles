#!/usr/bin/env bash

set -e

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
CONFIG=setup.conf.yaml
DOTBOT_DIR="dotbot"
DOTBOT_BIN="bin/dotbot"

git submodule update --init --recursive "${DOTBOT_DIR}"
"${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" -d "${BASEDIR}" -c "${CONFIG}" "${@}"

source env/by_hostname/`cat /etc/hostname`.env
"${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" -d "${BASEDIR}" -c "by_category_${DOTFILES_CATEGORY}.conf.yaml" "${@}"
